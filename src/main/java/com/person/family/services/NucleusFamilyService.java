package com.person.family.services;

import com.person.family.dto.NucleusFamilyDto;
import com.person.family.dto.PersonDto;
import com.person.family.exception.FamilyException;
import com.person.family.model.NucleusFamily;
import com.person.family.repository.NucleusFamilyRepository;
import com.person.family.util.TransformDtoToModel;
import com.person.family.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class NucleusFamilyService {

    @Autowired
    private NucleusFamilyRepository repository;

    @Autowired
    private PersonService personService;

    /*Metodo para guardar varios parientes
    * Valida que los datos sean ingresados
    * Valida que no existan parientos con el numero registrado
    * Valida que la persona exista */
    public boolean save(List<NucleusFamilyDto> familyDtos,String personDocument) throws FamilyException {
        for(NucleusFamilyDto dto:familyDtos){
            dto.setPersonDocument(personDocument);
            if(!Validation.validationNucleusFamily(dto)){
                throw new FamilyException("Debe ingresar todos los datos");
            }
        }
        PersonDto person=personService.finByDocument(personDocument);
        List<NucleusFamily> list=repository.findByPersonid(person.getId());
        for(NucleusFamilyDto dto:familyDtos){
            Optional<NucleusFamily> n=list.stream().filter(x->x.getNumber().equals(dto.getNumber())).findAny();
            if(n.isPresent()){
                throw new FamilyException("Ya existe un pariente registrado en el número "+n.get().getNumber());
            }
        }
        List<NucleusFamily> nucleusFamilies=familyDtos.stream().map(x-> TransformDtoToModel.transformNucleusFamilyDtoToNucleusFamily(x,person.getId())).collect(Collectors.toList());
        repository.saveAll(nucleusFamilies);
        return true;
    }

    public List<NucleusFamilyDto> finByPersonDocument(String document) throws FamilyException {
        PersonDto person=personService.finByDocument(document);
        List<NucleusFamily> list=repository.findByPersonid(person.getId());
        return list.stream().map(x->TransformDtoToModel.transformNucleusFamilyToNucleusFamilyDto(x,document)).collect(Collectors.toList());
    }

    /*Metodo para actualizar un parientes por numero y documento de persona
     * Valida que los datos sean ingresados
     * Valida que la persona exista
     * Valida que el numero del pariente exista */
    public boolean update(NucleusFamilyDto nucleusFamilyDto) throws FamilyException {
        if(!Validation.validationNucleusFamily(nucleusFamilyDto)){
           throw new FamilyException("Debe ingresar todos los datos");
        }
        PersonDto person=personService.finByDocument(nucleusFamilyDto.getPersonDocument());
        Optional<NucleusFamily> nucleusFamily=repository.findByNumberAndPersonid(nucleusFamilyDto.getNumber(),person.getId());
        NucleusFamily n=nucleusFamily.get();
        n.setName(nucleusFamilyDto.getName());
        n.setType_relation(nucleusFamilyDto.getTypeRelation());
        repository.save(n);
        return true;
    }

    public void delete(String personDocument, int number) throws FamilyException {
        PersonDto person=personService.finByDocument(personDocument);
        Optional<NucleusFamily> nucleusFamily=repository.findByNumberAndPersonid(number,person.getId());
        if(nucleusFamily.isPresent()){
            repository.delete(nucleusFamily.get());
        }else {
            throw new FamilyException("No se encontro un pariente asociado con ese numero");
        }

    }

    public void deleteByPersonId(Long personId){
        repository.deleteByPersonid(personId);
    }



}
