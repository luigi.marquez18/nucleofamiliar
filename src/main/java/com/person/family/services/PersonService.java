package com.person.family.services;

import com.person.family.dto.PersonDto;
import com.person.family.exception.FamilyException;
import com.person.family.model.Person;
import com.person.family.model.TypeDocument;
import com.person.family.repository.PersonRepository;
import com.person.family.util.TransformDtoToModel;
import com.person.family.util.Util;
import com.person.family.util.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonService {

    @Autowired
    private PersonRepository repository;

    @Autowired
    private TypeDocumentService typeDocumentService;

    @Autowired
    private NucleusFamilyService nucleusFamilyService;

    //Busca una persona por documento, si no lo encuentra retorna la excepcion personalizada
    public PersonDto finByDocument(String document) throws FamilyException {
        Optional<Person> personOptional=repository.findByDocument(document);
        if(personOptional.isPresent()){
            return TransformDtoToModel.transformPersonToPersonDto(personOptional.get());
        }else{
            throw new FamilyException("No se encontro persona con número de documento "+document);
        }
    }

    public PersonDto finByDocumentAndNucleusFamilyList(String document) throws FamilyException {
        PersonDto dto=finByDocument(document);
        dto.setNucleusFamilyList(nucleusFamilyService.finByPersonDocument(document));
        return dto;
    }

    /*Metodo que permite guardar una person,
      valida por documento, valida si una persona ya existe con ese documento
      validar que el tipo de documento sea el parametrizado por la base de datos
     */
    public PersonDto save(PersonDto personDto) throws FamilyException {
        if(Validation.validationPerson(personDto)){
            Optional<Person> personOptional=repository.findByDocument(personDto.getDocument());
            if (!personOptional.isPresent()) {
                List<TypeDocument> documents=typeDocumentService.findAll();
                Optional<TypeDocument> documentFound=documents.stream().filter( x->x.getId().equals(personDto.getTypeDocument())).findAny();
                if(documentFound.isPresent()){
                    Person person=repository.save(TransformDtoToModel.transformPersonDtoToPerson(personDto));
                    return TransformDtoToModel.transformPersonToPersonDto(person);
                }else{
                    throw new FamilyException("Tipo de documento incorrecto, documentos validos "+ Util.getValidTypeDocuments(documents));
                }
            } else {
                throw new FamilyException("Ya existe una persona con el mismo tipo de documento");
            }
        }else{
            throw new FamilyException("Debe ingresar todo los datos de la persona");
        }
    }

    public PersonDto update(PersonDto dto) throws FamilyException {
        if(Validation.validationPerson(dto)){
            PersonDto personDto=finByDocument(dto.getDocument());
            List<TypeDocument> documents=typeDocumentService.findAll();
            Optional<TypeDocument> documentFound=documents.stream().filter( x->x.getId().equals(dto.getTypeDocument())).findAny();
            if(documentFound.isPresent()){
                Person p=TransformDtoToModel.transformPersonDtoToPerson(dto);
                p.setId(personDto.getId());
                repository.save(p);
                return TransformDtoToModel.transformPersonToPersonDto(p);
            }else{
                throw new FamilyException("Tipo de documento incorrecto, documentos validos "+ Util.getValidTypeDocuments(documents));
            }

        }else{
            throw new FamilyException("Debe ingresar todo los datos de la persona");
        }
    }

    public void delete(String document) throws FamilyException {
        PersonDto personDto=finByDocument(document);
        nucleusFamilyService.deleteByPersonId(personDto.getId());
        repository.deleteById(personDto.getId());

    }


}
