package com.person.family.services;

import com.google.common.collect.Lists;
import com.person.family.model.TypeDocument;
import com.person.family.repository.TypeDocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TypeDocumentService {

    @Autowired
    TypeDocumentRepository repository;

    public Optional<TypeDocument> getTypeDocumentById(String id){
        return repository.findById(id);
    }

    public List<TypeDocument> findAll() {
        return repository.findAll();
    }
}
