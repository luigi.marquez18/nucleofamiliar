package com.person.family.services;

import com.person.family.model.Log;
import com.person.family.repository.LogRepository;
import com.person.family.util.LogTypes;
import com.person.family.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;



@Service
public class LogService {

    private static final Logger logger = LoggerFactory.getLogger(LogService.class);

    @Autowired
    private LogRepository repository;

    @Async
    public void generate(String service, String in,String out, LogTypes l) {
        try {
            Log log=new Log();
            log.setDate(Util.getCurrentDate());
            log.setType(l.name());
            log.setIndata(in);
            log.setOutdata(out);
            log.setService(service);
            repository.save(log);
        } catch (Exception e) {
            logger.error(Util.printLog("generateLog",e.getMessage()));
        }
    }
}
