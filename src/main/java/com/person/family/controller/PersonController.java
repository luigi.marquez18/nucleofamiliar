package com.person.family.controller;
import com.person.family.async.LogAsync;
import com.person.family.dto.PersonDto;
import com.person.family.exception.FamilyException;
import com.person.family.services.PersonService;
import com.person.family.util.LogTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("person")
public class PersonController {

    @Autowired
    private PersonService service;
    @Autowired
    private LogAsync logAsync;

    @PostMapping("/save")
    public @ResponseBody
    ResponseEntity save(@RequestBody PersonDto personDto) {
        String in=personDto.toString();
        try {
            PersonDto person=service.save(personDto);
            logAsync.generate("savePerson",in,person.toString(), LogTypes.OK);
            return new ResponseEntity<>(person, HttpStatus.OK);
        }catch (FamilyException e1){
            logAsync.generate("savePerson",in,e1.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e1.getMessage());
        }catch (Exception e){
            logAsync.generate("savePerson",in,e.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping("/finByDocument/{document}")
    public @ResponseBody
    ResponseEntity finByDocument(@PathVariable("document") String document){
        String in=document;
        try {
            PersonDto dto=service.finByDocumentAndNucleusFamilyList(document);
            logAsync.generate("findPersonByDocument",in,dto.toString(), LogTypes.OK);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        }catch (FamilyException e1){
            logAsync.generate("findPersonByDocument",in,e1.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e1.getMessage());
        }catch (Exception e){
            logAsync.generate("findPersonByDocument",in,e.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @PutMapping("/update")
    public @ResponseBody
    ResponseEntity update(@RequestBody PersonDto personDto) {
        String in=personDto.toString();
        try {
            PersonDto person=service.update(personDto);
            logAsync.generate("updatePerson",in,person.toString(), LogTypes.OK);
            return new ResponseEntity<>(person, HttpStatus.OK);
        }catch (FamilyException e1){
            logAsync.generate("updatePerson",in,e1.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e1.getMessage());
        }catch (Exception e){
            logAsync.generate("updatePerson",in,e.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @DeleteMapping("/delete/{document}")
    public @ResponseBody
    ResponseEntity delete(@PathVariable("document") String document){
       String in=document;
        try {
            service.delete(document);
            logAsync.generate("deletePerson",in,"Eliminado exitosamente", LogTypes.OK);
            return ResponseEntity.status(HttpStatus.OK).body("Eliminado exitosamente");
        }catch (FamilyException e1){
            logAsync.generate("deletePerson",in,e1.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e1.getMessage());
        }catch (Exception e){
            logAsync.generate("deletePerson",in,e.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
