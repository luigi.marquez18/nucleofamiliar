package com.person.family.controller;

import com.person.family.async.LogAsync;
import com.person.family.dto.NucleusFamilyDto;
import com.person.family.exception.FamilyException;
import com.person.family.services.NucleusFamilyService;
import com.person.family.util.LogTypes;
import com.person.family.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("nucleusFamily")
public class NucleusFamilyController {

    @Autowired
    private NucleusFamilyService service;
    @Autowired
    private LogAsync logAsync;

    @PostMapping("/save")
    public @ResponseBody
    ResponseEntity save(@RequestParam("personDocument") String  personDocument,@RequestBody List<NucleusFamilyDto> nucleusFamilyDtos) {
        String in=personDocument+"--"+ Util.dataFamily(nucleusFamilyDtos);
        try {
            service.save(nucleusFamilyDtos,personDocument);
            logAsync.generate("savePerson",in,Util.dataFamily(nucleusFamilyDtos)+"--Nucleo Familiar Agregado con Exito", LogTypes.OK);
            return ResponseEntity.status(HttpStatus.OK).body("Nucleo Familiar Agregado con Exito");
        }catch (FamilyException e1){
            logAsync.generate("saveNucleusFamily",in,e1.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e1.getMessage());
        }catch (Exception e){
            logAsync.generate("saveNucleusFamily",in,e.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @GetMapping("/finByPersonDocument/{document}")
    public @ResponseBody
    ResponseEntity finByDocument(@PathVariable("document") String document){
        String  in=document;
        try {
            List<NucleusFamilyDto> list=service.finByPersonDocument(document);
            logAsync.generate("findByDocumentNucleusFamily",in,Util.dataFamily(list), LogTypes.OK);
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch (FamilyException e1){
            logAsync.generate("findByDocumentNucleusFamily",in,e1.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e1.getMessage());
        }catch (Exception e){
            logAsync.generate("findByDocumentNucleusFamily",in,e.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    /*Actualiza por numero de pariente y documento de persona*/
    @PutMapping("/updateByNumberAndPersonDocument")
    public @ResponseBody
    ResponseEntity updateByNumberAndPersonDocument(@RequestBody NucleusFamilyDto familyDto){
        String in=familyDto.toString();
        try {
            service.update(familyDto);
            logAsync.generate("updateByNumberAndPersonDocument",in,familyDto.toString()+"---Actualizado exitosamente", LogTypes.OK);
            return ResponseEntity.status(HttpStatus.OK).body("Actualizado exitosamente");
        }catch (FamilyException e1){
            logAsync.generate("updateByNumberAndPersonDocument",in,e1.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e1.getMessage());
        }catch (Exception e){
            logAsync.generate("updateByNumberAndPersonDocument",in,e.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    /*Eliminar por numero de pariente y documento de persona*/
    @DeleteMapping("/deleteByNumberAndPersonDocument/{number}/{personDocument}")
    public @ResponseBody
    ResponseEntity deleteByNumberAndPersonDocument(@PathVariable("number") int number,@PathVariable("personDocument") String document){
        String in=number+"--"+document;
        try {
            service.delete(document,number);
            logAsync.generate("deleteByNumberAndPersonDocument",in,number+"--"+document+"--"+"Eliminado exitosamente", LogTypes.OK);
            return ResponseEntity.status(HttpStatus.OK).body("Eliminado exitosamente");
        }catch (FamilyException e1){
            logAsync.generate("deleteByNumberAndPersonDocument",in,e1.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.CONFLICT).body(e1.getMessage());
        }catch (Exception e){
            logAsync.generate("deleteByNumberAndPersonDocument",in,e.getMessage(), LogTypes.ERROR);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }
}
