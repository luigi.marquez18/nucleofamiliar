package com.person.family.async;

import com.person.family.model.Log;
import com.person.family.services.LogService;
import com.person.family.util.LogTypes;
import com.person.family.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

@Service
@EnableAsync
public class LogAsync {

    @Autowired
    private LogService logService;

    public void generate(String service, String in,String out, LogTypes l){
        logService.generate(service,in,out,l);
    }
}
