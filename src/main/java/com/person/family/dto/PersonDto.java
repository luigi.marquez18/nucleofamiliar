package com.person.family.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class PersonDto implements Serializable {
    private Long id;
    private String document;
    private String firstName;
    private String lastName;
    private String typeDocument;
    private String birthDate;
    private String phone;
    private String email;
    private List<NucleusFamilyDto> nucleusFamilyList;

    @Override
    public String toString() {
        return "PersonDto{" +
                "id=" + id +
                ", document='" + document + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", typeDocument='" + typeDocument + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
