package com.person.family.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class NucleusFamilyDto implements Serializable {
    private Integer number;
    private String name;
    private String typeRelation;
    private String personDocument;

    @Override
    public String toString() {
        return "NucleusFamilyDto{" +
                "number=" + number +
                ", name='" + name + '\'' +
                ", typeRelation='" + typeRelation + '\'' +
                ", personDocument='" + personDocument + '\'' +
                '}';
    }
}
