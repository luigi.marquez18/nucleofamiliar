package com.person.family.exception;

public class FamilyException extends Exception {

    public FamilyException(String message) {
        super(message);
    }
}
