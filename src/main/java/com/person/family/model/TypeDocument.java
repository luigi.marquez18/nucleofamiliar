package com.person.family.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "type_document")
@Getter
@Setter
public class TypeDocument {
    @Id
    private String id;
    private String description;
}
