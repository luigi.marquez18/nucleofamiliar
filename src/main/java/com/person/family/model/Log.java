package com.person.family.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "log")
@Getter
@Setter
public class Log {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String service;
    private String type;
    private String indata;
    private String outdata;
    private String date;
}
