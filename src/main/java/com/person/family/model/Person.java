package com.person.family.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "person")
@Getter
@Setter
public class Person {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String document;
    private String firstname;
    private String lastname;
    private String type_document;
    private String birth_date;
    private String phone;
    private String email;
}
