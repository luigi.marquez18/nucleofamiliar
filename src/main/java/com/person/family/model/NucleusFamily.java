package com.person.family.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "nucleus_family")
@Getter
@Setter
public class NucleusFamily {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private Integer number;
    private String name;
    private String type_relation;
    private Long personid;
}
