package com.person.family.repository;

import com.person.family.model.NucleusFamily;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
public interface NucleusFamilyRepository extends JpaRepository<NucleusFamily, Long> {
    List<NucleusFamily> findByPersonid(Long personid);
    Optional<NucleusFamily> findByNumberAndPersonid(Integer number,Long personid);
    void deleteByPersonid(Long personid);
}
