package com.person.family.util;

import com.person.family.dto.NucleusFamilyDto;
import com.person.family.dto.PersonDto;
import com.person.family.model.NucleusFamily;
import com.person.family.model.Person;

/* Clase para transformar Dto a Model, ya que no son tantos
modelos  se utiliza una sola clase para tosas las transformaciones */
public class TransformDtoToModel {

    /*Función que permite transforma un objeto PersonDto a un objeto Person*/
    public static Person transformPersonDtoToPerson(PersonDto dto){
        Person person=new Person();
        person.setDocument(dto.getDocument());
        person.setBirth_date(dto.getBirthDate());
        person.setEmail(dto.getEmail());
        person.setFirstname(dto.getFirstName());
        person.setLastname(dto.getLastName());
        person.setPhone(dto.getPhone());
        person.setType_document(dto.getTypeDocument());
        person.setId(dto.getId());
        return person;
    }

    /*Función que permite transforma un objeto Person a un objeto PersonDTO*/
    public static PersonDto transformPersonToPersonDto(Person person){
        PersonDto personDto=new PersonDto();
        personDto.setDocument(person.getDocument());
        personDto.setBirthDate(person.getBirth_date());
        personDto.setEmail(person.getEmail());
        personDto.setFirstName(person.getFirstname());
        personDto.setLastName(person.getLastname());
        personDto.setPhone(person.getPhone());
        personDto.setTypeDocument(person.getType_document());
        personDto.setId(person.getId());
        return personDto;
    }

    /*Función que permite transforma un objeto NucleusFamilyDto a un objeto NucleusFamily*/
    public static NucleusFamily transformNucleusFamilyDtoToNucleusFamily(NucleusFamilyDto dtp,Long personId){
        NucleusFamily family=new NucleusFamily();
        family.setNumber(dtp.getNumber());
        family.setType_relation(dtp.getTypeRelation());
        family.setName(dtp.getName());
        family.setPersonid(personId);
        return family;
    }

    /*Función que permite transforma un objeto NucleusFamily a un objeto NucleusFamilyDto*/
    public static NucleusFamilyDto transformNucleusFamilyToNucleusFamilyDto(NucleusFamily n,String personDocument){
        NucleusFamilyDto family=new NucleusFamilyDto();
        family.setNumber(n.getNumber());
        family.setTypeRelation(n.getType_relation());
        family.setName(n.getName());
        family.setPersonDocument(personDocument);
        return family;
    }
}
