package com.person.family.util;

import com.person.family.dto.NucleusFamilyDto;
import com.person.family.dto.PersonDto;

public class Validation {

    /* Metodo que valida que todos los campos del objeto person sean ingresado */
    public static boolean validationPerson(PersonDto dto){
        String[] values=new String[]{dto.getDocument(),dto.getFirstName(),dto.getLastName(), dto.getBirthDate(),
        dto.getEmail(),dto.getPhone(),dto.getTypeDocument()};
        return (Util.isValidFields(values));
    }

    /* Metodo que valida que todos los campos del objeto nucleo familiar sean ingresado */
    public static boolean validationNucleusFamily(NucleusFamilyDto dto){
        String[] values=new String[]{dto.getName(),dto.getTypeRelation(), dto.getPersonDocument()};
        Integer[] valuesI=new Integer[]{dto.getNumber()};
        return (Util.isValidFields(values) && Util.isValidFields(valuesI));
    }
}
