package com.person.family.util;

import com.person.family.dto.NucleusFamilyDto;
import com.person.family.model.TypeDocument;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Util {


    public static boolean isValidFields(String[] data){
        for(String d:data){
            if(d==null || d.isEmpty()){
                return false;
            }
        }
        return true;
    }

    public static boolean isValidFields(Integer[] data){
        for(Integer d:data){
            if(d==null){
                return false;
            }
        }
        return true;
    }

    /* Retorna los tipos validos de documentos para mostrar por pantalla */
    public static String getValidTypeDocuments(List<TypeDocument> documents){
        String list="";
        for(TypeDocument document:documents){
            list+=(list.isEmpty())?document.getId():","+document.getId();
        }
        return list;
    }

    public static String getCurrentDate(){
        Date d=new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        return dateFormat.format(d);
    }

    public static String printLog(String TAG,String message){
        return TAG+" - "+message;
    }

    public static String dataFamily(List<NucleusFamilyDto> list){
        String data="";
        for (NucleusFamilyDto dto:list) {
            data+=(data.isEmpty())?dto.toString():","+dto.toString();
        }
        return data;
    }
}
