/*
SQLyog Ultimate v8.82 
MySQL - 5.7.26-log : Database - family
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`family` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci */;

USE `family`;

/*Table structure for table `log` */

DROP TABLE IF EXISTS `log`;

CREATE TABLE `log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `type` char(5) COLLATE latin1_spanish_ci DEFAULT NULL,
  `indata` longtext COLLATE latin1_spanish_ci,
  `outdata` longtext COLLATE latin1_spanish_ci,
  `date` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `log` */

insert  into `log`(`id`,`service`,`type`,`indata`,`outdata`,`date`) values (21,'savePerson','ERROR','PersonDto{id=null, document=\'1245689\', firstName=\'Luis Davia\', lastName=\'Otoniel Ibañez\', typeDocument=\'CC\', birthDate=\'1992-07-06\', phone=\'3015939215\', email=\'luis.gomez@gmail.com\'}','Ya existe una persona con el mismo tipo de documento','2020/07/20 06:17:55'),(22,'savePerson','OK','PersonDto{id=null, document=\'124568955\', firstName=\'Luis Jose\', lastName=\'Otoniel Ibañez\', typeDocument=\'CC\', birthDate=\'1992-07-06\', phone=\'3015939215\', email=\'luis.gomez@gmail.com\'}','PersonDto{id=10, document=\'124568955\', firstName=\'Luis Jose\', lastName=\'Otoniel Ibañez\', typeDocument=\'CC\', birthDate=\'1992-07-06\', phone=\'3015939215\', email=\'luis.gomez@gmail.com\'}','2020/07/20 06:18:33'),(23,'findPersonByDocument','OK','1245671','PersonDto{id=5, document=\'1245671\', firstName=\'Luigy Enrique\', lastName=\'Marquez Ibañez\', typeDocument=\'CC\', birthDate=\'1992-07-04\', phone=\'3015939205\', email=\'luigi.marquez18@gmail.com\'}','2020/07/20 06:18:37'),(24,'updatePerson','OK','PersonDto{id=null, document=\'1245671\', firstName=\'Luigy Enrique\', lastName=\'Marquez Ibañez\', typeDocument=\'CC\', birthDate=\'1992-07-04\', phone=\'3015939205\', email=\'luigi.marquez18@gmail.com\'}','PersonDto{id=5, document=\'1245671\', firstName=\'Luigy Enrique\', lastName=\'Marquez Ibañez\', typeDocument=\'CC\', birthDate=\'1992-07-04\', phone=\'3015939205\', email=\'luigi.marquez18@gmail.com\'}','2020/07/20 06:18:41'),(25,'deletePerson','OK','124568955','Eliminado exitosamente','2020/07/20 06:18:51'),(26,'savePerson','OK','1245671--NucleusFamilyDto{number=5, name=\'Manuel Marquez\', typeRelation=\'Tio\', personDocument=\'null\'}','NucleusFamilyDto{number=5, name=\'Manuel Marquez\', typeRelation=\'Tio\', personDocument=\'1245671\'}--Nucleo Familiar Agregado con Exito','2020/07/20 06:19:44'),(27,'findPersonByDocument','OK','1245671','PersonDto{id=5, document=\'1245671\', firstName=\'Luigy Enrique\', lastName=\'Marquez Ibañez\', typeDocument=\'CC\', birthDate=\'1992-07-04\', phone=\'3015939205\', email=\'luigi.marquez18@gmail.com\'}','2020/07/20 06:19:49'),(28,'findByDocumentNucleusFamily','OK','1245671','NucleusFamilyDto{number=1, name=\'Linda katherine Marquez Ibañez\', typeRelation=\'Hermana\', personDocument=\'1245671\'},NucleusFamilyDto{number=2, name=\'Dency Marquez\', typeRelation=\'Padre\', personDocument=\'1245671\'},NucleusFamilyDto{number=3, name=\'Isabel Ibañez\', typeRelation=\'Madre\', personDocument=\'1245671\'},NucleusFamilyDto{number=5, name=\'Manuel Marquez\', typeRelation=\'Tio\', personDocument=\'1245671\'}','2020/07/20 06:19:57'),(29,'deleteByNumberAndPersonDocument','OK','5--1245671','5--1245671--Eliminado exitosamente','2020/07/20 06:20:28'),(30,'findPersonByDocument','OK','1245671','PersonDto{id=5, document=\'1245671\', firstName=\'Luigy Enrique\', lastName=\'Marquez Ibañez\', typeDocument=\'CC\', birthDate=\'1992-07-04\', phone=\'3015939205\', email=\'luigi.marquez18@gmail.com\'}','2020/07/20 06:20:32'),(31,'savePerson','OK','PersonDto{id=null, document=\'124568955\', firstName=\'Luis Jose\', lastName=\'Otoniel Ibañez\', typeDocument=\'CC\', birthDate=\'1992-07-06\', phone=\'3015939215\', email=\'luis.gomez@gmail.com\'}','PersonDto{id=11, document=\'124568955\', firstName=\'Luis Jose\', lastName=\'Otoniel Ibañez\', typeDocument=\'CC\', birthDate=\'1992-07-06\', phone=\'3015939215\', email=\'luis.gomez@gmail.com\'}','2020/07/20 06:20:38'),(32,'savePerson','ERROR','PersonDto{id=null, document=\'124568955\', firstName=\'Luis Jose\', lastName=\'Otoniel Ibañez\', typeDocument=\'CC\', birthDate=\'1992-07-06\', phone=\'3015939215\', email=\'luis.gomez@gmail.com\'}','Ya existe una persona con el mismo tipo de documento','2020/07/20 06:21:02');

/*Table structure for table `nucleus_family` */

DROP TABLE IF EXISTS `nucleus_family`;

CREATE TABLE `nucleus_family` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `name` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `type_relation` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `personid` bigint(20) NOT NULL,
  PRIMARY KEY (`id`,`number`,`personid`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `nucleus_family` */

insert  into `nucleus_family`(`id`,`number`,`name`,`type_relation`,`personid`) values (1,1,'Linda katherine Marquez Ibañez','Hermana',5),(3,2,'Dency Marquez','Padre',5),(4,3,'Isabel Ibañez','Madre',5);

/*Table structure for table `person` */

DROP TABLE IF EXISTS `person`;

CREATE TABLE `person` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `document` char(11) COLLATE latin1_spanish_ci NOT NULL,
  `firstname` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `lastname` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `type_document` char(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `birth_date` char(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `email` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`,`document`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `person` */

insert  into `person`(`id`,`document`,`firstname`,`lastname`,`type_document`,`birth_date`,`phone`,`email`) values (4,'1245672','Enrique','Ibañez','CC','Enrique','3015939205','enrique.marquez@gmail.com'),(5,'1245671','Luigy Enrique','Marquez Ibañez','CC','1992-07-04','3015939205','luigi.marquez18@gmail.com'),(8,'1245688','Luis Javier','Gomez Ibañez','CC','1992-07-06','3015939215','luis.gomez@gmail.com'),(9,'1245689','Luis Davia','Otoniel Ibañez','CC','1992-07-06','3015939215','luis.gomez@gmail.com'),(11,'124568955','Luis Jose','Otoniel Ibañez','CC','1992-07-06','3015939215','luis.gomez@gmail.com');

/*Table structure for table `type_document` */

DROP TABLE IF EXISTS `type_document`;

CREATE TABLE `type_document` (
  `id` char(10) COLLATE latin1_spanish_ci NOT NULL,
  `description` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

/*Data for the table `type_document` */

insert  into `type_document`(`id`,`description`) values ('CC','Cedula Ciudadania'),('CE','Cedular Extranjera'),('PP','Pasaporte'),('TI','Tarjeta de Identidad');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
